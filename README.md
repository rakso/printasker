# Printasker

Store your tasks in Google Keep and print them on thermal printer! Simple application that creates HTTP server that allows to create notes in Google Keep with specific tag and then print them on USB thermal printer (like ZJ-5890K).

# Getting started

Create virtual environment `python3 -m venv .venv`, then activate it. Instal required packages `pip install -r requirements.txt`.

Run the server: `python server.py`

_TBA: How to configure printer in WSL, how to obtain the google master token._
