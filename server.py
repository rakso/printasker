#!venv/bin/python

from flask import Flask, request, jsonify, send_from_directory, Response

from lib.keepClient import KeepClient, NotInitiatedException
from lib.config import AppConfig

from json import dumps


app = Flask(__name__)


config = AppConfig()
token = config.getKeepMasterToken()
email = config.getKeepUserEmail()

keepClient = KeepClient(token, email)


@app.route('/api/init')
def generate_large_csv():
    keepClient.init()
    return jsonify({'message': 'authenticated'}), 200


@app.route('/api/create', methods=['GET', 'POST'])
def createNoteController():
    if request.method == 'POST':
        data = request.json
        noteContent = data.get('content')
        reinitPrinter = data.get('reinitPrinter')
        mimetype = 'application/x-ndjson'
    elif request.method == 'GET':
        noteContent = request.args.get('content')
        reinitPrinter = request.args.get('reinitPrinter')
        mimetype = None  # When mimetype is set to "text/plain" or JSON i don't see the live view in chrome browser
    else:
        return jsonify({'error': 'Method not allowed'}), 405

    def createNote(noteContent, reinitPrinter):
        print(reinitPrinter)
        if reinitPrinter:
            keepClient.resetPrinterInit()

        try:
            if not keepClient.isInitiated():
                raise NotInitiatedException

            yield dumps({'status': 'CREATING_NOTE'}) + "\n"

            try:
                createdNote = keepClient.add(noteContent)
                yield dumps({'status': 'NOTE_CREATED', 'note': createdNote}) + "\n"
            except Exception as e:
                yield dumps({'status': 'ERROR', 'error': f'Note was not saved. Error: {e}'}) + "\n"

        except NotInitiatedException:
            print('Client is not initiated. Initiating...')
            yield dumps({'status': 'AUTHENTICATING'}) + "\n"
            keepClient.init()
            yield dumps({'status': 'AUTHENTICATED'}) + "\n"
            yield dumps({'status': 'CREATING_NOTE'}) + "\n"

            try:
                createdNote = keepClient.add(noteContent)
                yield dumps({'status': 'NOTE_CREATED', 'note': createdNote}) + "\n"
            except Exception as e:
                yield dumps({'status': 'ERROR', 'error': f'{e}'}) + "\n"

    return Response(createNote(noteContent, reinitPrinter), mimetype=mimetype)


@ app.route('/<path:path>')
@ app.route('/')
def send_index(path='index.html'):
    return send_from_directory('static', path)


if __name__ == '__main__':
    keepClient.init()
    app.run(port=8141)
