from escpos import *
from datetime import datetime
from unidecode import unidecode


class PrintNote:
    def __init__(self):
        pass

    def connect(self):
        # self.__printer = printer.Usb(0x0416, 0x5011, profile='ZJ-5870')
        self.__printer = printer.Usb(0x0416, 0x5011)

    def printNote(self, title, content):
        self.__printer.set('center', 'a', invert=True)
        self.__printer.text(f'  {title}  \n\n')
        self.__printer.set('left', 'a')
        self.__printer.text(f'{unidecode(content)}\n')
        self.__printer.set('right', 'a')
        self.__printer.text(f'\n\n__________________\n')
        self.__printer.set('right', 'b')

        now = datetime.now()
        date_time = now.strftime("%d/%m/%Y, %H:%M:%S")

        self.__printer.text(f'{date_time}')
        self.__printer.cut()
