from os import environ as env
from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.


class AppConfig:
    def __init__(self):
        self.__value = None
        pass

    def getValue(self, key, default=None):
        self.__value = env.get(key, default)
        return self

    def __required(self):
        if self.__value is None:
            raise Exception('Value is not set, but it is required.')

    def getKeepUserEmail(self):
        self.getValue('KEEP_USER_EMAIL').__required()
        return self.__value

    def getKeepMasterToken(self):
        self.getValue('KEEP_MASTER_TOKEN').__required()
        return self.__value
