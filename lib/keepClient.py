import gkeepapi
from time import time
from .thermalPrinter import PrintNote
import json


class NotInitiatedException(Exception):
    "Raised when the google keep is not initiated."
    pass


class KeepClient:
    def __init__(self, masterToken, keepUserEmail):
        self.__masterToken = masterToken
        self.__keepUserEmail = keepUserEmail

        self.__keep = gkeepapi.Keep()
        self.__initiated = False
        self.__printerInitiated = False

        self.__printNote = PrintNote()

    def resetPrinterInit(self):
        self.__printerInitiated = False
        self.initPrinter()

    def initPrinter(self):
        if not self.__printerInitiated:
            self.__printNote.connect()
            self.__printerInitiated = True

    def isInitiated(self):
        return self.__initiated

    def init(self):
        print('Authenticating..', end='', flush=True)
        try:
            fh = open('state.json', 'r')
            state = json.load(fh)
        except FileNotFoundError:
            state = None

        self.__keep.authenticate(self.__keepUserEmail,
                                 self.__masterToken, state)

        state = self.__keep.dump()
        fh = open('state.json', 'w')
        json.dump(state, fh)
        print('.. success!')

        self.label = self.__keep.findLabel('1. ToDo')

        self.__initiated = True

    def add(self, content=None):
        if not self.isInitiated():
            raise NotInitiatedException('Not initiated. Please .init() first.')

        self.initPrinter()

        title = f'ZD{int(time())}'

        if content is None:
            content = input("Note? > ")

        print('Printing..', end='', flush=True)
        self.__printNote.printNote(title, content)
        print('.. done!')

        note = self.__keep.createNote(title, content)

        note.labels.add(self.label)
        note.color = gkeepapi.node.ColorValue.Red
        print('Saving..', end='', flush=True)
        self.__keep.sync()
        print('.. done!')

        return {'title': title, 'content': content}
